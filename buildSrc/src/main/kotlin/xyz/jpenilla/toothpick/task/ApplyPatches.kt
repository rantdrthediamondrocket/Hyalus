/*
 * This file is part of Toothpick, licensed under the MIT License.
 *
 * Copyright (c) 2020-2021 Jason Penilla & Contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package xyz.jpenilla.toothpick.task

import org.gradle.api.tasks.TaskAction
import xyz.jpenilla.toothpick.ensureSuccess
import xyz.jpenilla.toothpick.gitCmd
import xyz.jpenilla.toothpick.reEnableGitSigning
import xyz.jpenilla.toothpick.rootProjectDir
import xyz.jpenilla.toothpick.temporarilyDisableGitSigning
import xyz.jpenilla.toothpick.bashCmd
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import xyz.jpenilla.toothpick.toothpick

public open class ApplyPatches : ToothpickTask() {
  public companion object {
    /**
     * The default git arguments to apply patches.
     *
     * `listOf("am", "--3way", "--ignore-whitespace")`
     */
    public val DEFAULT_APPLY_COMMAND: List<String> = listOf("am", "--3way", "--ignore-whitespace")
  }

  private val applyCommand = ArrayList(DEFAULT_APPLY_COMMAND)

  public fun applyCommand(vararg gitArguments: String): Unit =
    applyCommand(gitArguments.toList())

  public fun applyCommand(gitArguments: List<String>) {
    applyCommand.clear()
    applyCommand.addAll(gitArguments)
  }

  public fun applyPatchesYarn(): Boolean { // Todo actually port to kotlin
      val forkName = toothpick.forkName
      val projectDir = Paths.get("${project.getRootDir()}/$forkName-Server_yarn").toFile()
      val importDir = Paths.get("${project.getRootDir()}/mappings/work/$forkName-Server_yarn_unpatched").toFile()
      logger.lifecycle(">>> Resetting subproject $name")
      if (projectDir.exists()) {
          ensureSuccess(gitCmd("fetch", "origin", dir = projectDir))
          ensureSuccess(gitCmd("reset", "--hard", "origin/master", dir = projectDir))
      } else {
          ensureSuccess(gitCmd("clone", importDir.toString(), projectDir.toString(), printOut = true, dir = project.getRootDir()))
          ensureSuccess(gitCmd("checkout", "-b", "upstream/upstream", printOut = true, dir = projectDir))
          ensureSuccess(gitCmd("checkout", "master", printOut = true, dir = projectDir))
      }
      logger.lifecycle(">>> Done resetting subproject $name")

      projectDir.mkdirs()
      val applyName = "mappedPatches"
      val name = "$forkName-Server_yarn"
      val patchDir: Path = Paths.get("${project.getRootDir()}/mappedPatches")
      if (Files.notExists(patchDir)) return true


      val patchPaths = Files.newDirectoryStream(patchDir)
          .map { it.toFile() }
          .filter { it.name.endsWith(".patch") }
          .sorted()
          .takeIf { it.isNotEmpty() } ?: return true
      val patches = patchPaths.map { it.absolutePath }.toTypedArray()

      logger.lifecycle(">>> Applying $applyName patches to $name")

      gitCmd("am", "--abort", dir = projectDir)

      val gitCommand = arrayListOf("am", "--3way", "--ignore-whitespace", "--rerere-autoupdate", "--whitespace=fix",  *patches)

      if (gitCmd(*gitCommand.toTypedArray(), dir = projectDir, printOut = true).exitCode != 0) {
          gitCmd("add", ".", dir = projectDir, printOut = true)
          gitCmd("am", "--continue", dir = projectDir, printOut = true)
      }
      return false;
  }

  public fun initYarn() { // Todo actually port to kotlin
      val paperDecompDir = toothpick.paperDecompDir
      val forkName = toothpick.forkName
      bashCmd("cd mappings/mapper && ./gradlew installDist", printOut = true, dir = project.getRootDir())
      bashCmd("rm -fr mappings/work/Base", printOut = true, dir = project.getRootDir())
      bashCmd("mkdir -p mappings/work/Base/src/main/java/com/mojang", printOut = true, dir = project.getRootDir())
      bashCmd("cp -r $paperDecompDir/spigot/* mappings/work/Base/src/main/java/", printOut = true, dir = project.getRootDir())
      
      
      val patchPaths = Files.newDirectoryStream(Paths.get("${project.getRootDir()}/patches/server"))
        .map { it.toFile() }
        .filter { it.name.endsWith(".patch") }
        .sorted()
        .toList()

      val linesRaw = patchPaths.asSequence()
      .flatMap { it.readLines().asSequence() }

      val lines = linesRaw
      .filter { line -> line.startsWith("--- a/src/main/java/") && !linesRaw.contains("+++ b/${line.substringAfter("""-- a/""")}") }
      .toList().distinct()
      
      val removedFiles = lines.asSequence()
        .filter { line -> line.startsWith("--- a/src/main/java/") }
        .distinct()
        .map { it.substringAfter("/src/main/java/") }
        .toSet()

      bashCmd("cp -r $forkName-Server/src/main/java/* mappings/work/Base/src/main/java/", printOut = true, dir = project.getRootDir())
      bashCmd("cp -r $paperDecompDir/libraries/com.mojang/*/* mappings/work/Base/src/main/java/", printOut = true, dir = project.getRootDir())

      removedFiles.forEach{ bashCmd("rm -frv mappings/work/Base/src/main/java/$it", printOut = true, dir = project.getRootDir()) }

      bashCmd("rm -fr mappings/work/$forkName-Server_yarn_unpatched && mkdir -p mappings/work/$forkName-Server_yarn_unpatched/src/main/java", printOut = true, dir = project.getRootDir())
      bashCmd("cp $forkName-Server/.gitignore $forkName-Server/pom.xml $forkName-Server/checkstyle.xml $forkName-Server/CONTRIBUTING.md $forkName-Server/LGPL.txt $forkName-Server/LICENCE.txt $forkName-Server/README.md mappings/work/$forkName-Server_yarn_unpatched/", printOut = true, dir = project.getRootDir())
      bashCmd("JAVA_OPTS='-Xms1G -Xmx2G' mappings/mapper/build/install/mapper/bin/mapper mappings/map.srg mappings/work/Base/src/main/java mappings/work/$forkName-Server_yarn_unpatched/src/main/java", printOut = true, dir = project.getRootDir())
      bashCmd("find -name '*.java' | xargs --max-procs=4 --no-run-if-empty sed -i '/^import [a-zA-Z0-9]*;$/d'", dir = File("${project.getRootDir()}/mappings/work/$forkName-Server_yarn_unpatched/src/main/java"))
      bashCmd("git init && git add . && git commit --quiet --message=init", dir = File("${project.getRootDir()}/mappings/work/$forkName-Server_yarn_unpatched"), printOut = true)
  }

  @TaskAction
  private fun applyPatches() {
    for (subproject in toothpick.subprojects) {
      val (baseDir, projectDir, patchesDir) = subproject
      val name = projectDir.name

      // Reset or initialize subproject
      logger.lifecycle(">>> Resetting subproject $name")
      if (projectDir.exists()) {
        ensureSuccess(gitCmd("fetch", "origin", dir = projectDir))
        ensureSuccess(gitCmd("reset", "--hard", "origin/master", dir = projectDir))
      } else {
        ensureSuccess(gitCmd("clone", baseDir.absolutePath, projectDir.absolutePath, dir = project.project.getRootDir()))
      }
      logger.lifecycle(">>> Done resetting subproject $name")

      if (!patchesDir.exists()) {
        logger.lifecycle(">>> Patches directory for $name does not exist, skipping")
        continue
      }
      // Apply patches
      val patchPaths = Files.newDirectoryStream(patchesDir.toPath())
        .map { it.toFile() }
        .filter { it.name.endsWith(".patch") }
        .sorted()
        .takeIf { it.isNotEmpty() } ?: continue
      val patches = patchPaths.map { it.absolutePath }.toTypedArray()

      val wasGitSigningEnabled = temporarilyDisableGitSigning(projectDir)

      logger.lifecycle(">>> Applying patches to $name")

      val gitCommand = ArrayList<String>()
      gitCommand.addAll(applyCommand)
      gitCommand.addAll(patches)
      ensureSuccess(gitCmd(*gitCommand.toTypedArray(), dir = projectDir, printOut = true)) {
        if (wasGitSigningEnabled) reEnableGitSigning(projectDir)
      }

      if (wasGitSigningEnabled) reEnableGitSigning(projectDir)
      logger.lifecycle(">>> Done applying patches to $name")
    }
    bashCmd("rm -fr patches/server/*-Mapped-Patches.patch", dir = project.getRootDir())

    initYarn()
    if (applyPatchesYarn()) {}
  }
}
