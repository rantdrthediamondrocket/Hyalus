From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Simon Gardling <titaniumtown@gmail.com>
Date: Mon, 3 May 2021 16:49:41 -0400
Subject: [PATCH] Better handling of async tasks

Replaces niche/one time use threadpools in other areas by instead using `asyncExecutor` in `MCUtil` This patch also adds a system of flushing small async tasks (hence the variable name of `smallAsyncTasks`) in the `asyncExecutor` threadpool.

diff --git a/src/main/java/net/minecraft/commands/CommandDispatcher.java b/src/main/java/net/minecraft/commands/CommandDispatcher.java
index a4b8e6d51def1fc039bfb933d3e74b7c64e4de44..a77e8ce4b50d531dd76298b278ba3578c6d8ecdc 100644
--- a/src/main/java/net/minecraft/commands/CommandDispatcher.java
+++ b/src/main/java/net/minecraft/commands/CommandDispatcher.java
@@ -339,8 +339,11 @@ final public class CommandDispatcher {
         // CraftBukkit start
         // Register Vanilla commands into builtRoot as before
         // Paper start - Async command map building
-        java.util.concurrent.ForkJoinPool.commonPool().execute(() -> {
-            sendAsync(entityplayer);
+        // java.util.concurrent.ForkJoinPool.commonPool().execute(() -> {
+        net.minecraft.server.MCUtil.smallAsyncTasks.add(new Runnable() {
+            public void run() {
+                sendAsync(entityplayer);
+            }
         });
     }
 
diff --git a/src/main/java/net/minecraft/server/MCUtil.java b/src/main/java/net/minecraft/server/MCUtil.java
index 5423403f005e47d0f53e9af0a459bfcb945e01ba..d76ad60764bb003ff172954f2c0f91db395d02ca 100644
--- a/src/main/java/net/minecraft/server/MCUtil.java
+++ b/src/main/java/net/minecraft/server/MCUtil.java
@@ -53,12 +53,15 @@ import java.util.concurrent.atomic.AtomicBoolean;
 import java.util.function.BiConsumer;
 import java.util.function.Consumer;
 import java.util.function.Supplier;
+import java.util.concurrent.SynchronousQueue;
+import java.util.concurrent.ConcurrentLinkedQueue;
 
 public final class MCUtil {
+    public static final ConcurrentLinkedQueue smallAsyncTasks = new ConcurrentLinkedQueue();
     public static final double COLLISION_EPSILON = 1.0E-7; // Tuinity - Just in case mojang changes this...
     public static final ThreadPoolExecutor asyncExecutor = new ThreadPoolExecutor(
-        0, 2, 60L, TimeUnit.SECONDS,
-        new LinkedBlockingQueue<Runnable>(),
+        4, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS,
+        new SynchronousQueue<Runnable>(),
         new ThreadFactoryBuilder().setDaemon(true).setNameFormat("Paper Async Task Handler Thread - %1$d").build() // Yatopia - daemon
     );
     public static final ThreadPoolExecutor cleanerExecutor = new ThreadPoolExecutor(
@@ -69,6 +72,29 @@ public final class MCUtil {
 
     public static final long INVALID_CHUNK_KEY = getCoordinateKey(Integer.MAX_VALUE, Integer.MAX_VALUE);
 
+    public static void flushAsyncTasks() {
+        if (!smallAsyncTasks.isEmpty()) {
+            asyncExecutor.submit(() -> {
+            Runnable runnable;
+            while((runnable = (Runnable)smallAsyncTasks.poll()) != null) {
+                    runnable.run();
+                }
+            });
+        }
+    }
+
+    public static void flushAsyncTasksMidTick() {
+        if (smallAsyncTasks.size() <= 16) {
+            asyncExecutor.submit(() -> {
+                Runnable runnable;
+                while((runnable = (Runnable)smallAsyncTasks.poll()) != null) {
+                    runnable.run();
+                }
+
+            });
+        }
+    }
+
 
     public static Runnable once(Runnable run) {
         AtomicBoolean ran = new AtomicBoolean(false);
diff --git a/src/main/java/net/minecraft/server/MinecraftServer.java b/src/main/java/net/minecraft/server/MinecraftServer.java
index 68a7f47b3de5ac5041fcd3ca89822339ac0e14cb..83b2243869b34d495fd331564af6c6f5f9311857 100644
--- a/src/main/java/net/minecraft/server/MinecraftServer.java
+++ b/src/main/java/net/minecraft/server/MinecraftServer.java
@@ -975,6 +975,7 @@ public abstract class MinecraftServer extends IAsyncTaskHandlerReentrant<Runnabl
         LOGGER.info("Flushing Chunk IO");
         com.destroystokyo.paper.io.PaperFileIOThread.Holder.INSTANCE.close(true, true); // Paper
         LOGGER.info("Closing Thread Pool");
+        MCUtil.flushAsyncTasks();
         SystemUtils.shutdownServerThreadPool(); // Paper
         LOGGER.info("Closing Server");
         try {
@@ -1283,6 +1284,7 @@ public abstract class MinecraftServer extends IAsyncTaskHandlerReentrant<Runnabl
             return;
         }
 
+        MCUtil.flushAsyncTasksMidTick();
         co.aikar.timings.MinecraftTimings.midTickChunkTasks.startTiming();
         try {
             for (;;) {
@@ -1491,6 +1493,7 @@ public abstract class MinecraftServer extends IAsyncTaskHandlerReentrant<Runnabl
 
         // Paper start - move executeAll() into full server tick timing
         try (co.aikar.timings.Timing ignored = MinecraftTimings.processTasksTimer.startTiming()) {
+            MCUtil.flushAsyncTasks();
             this.executeAll();
         }
         // Paper end
diff --git a/src/main/java/net/minecraft/server/network/LoginListener.java b/src/main/java/net/minecraft/server/network/LoginListener.java
index a1b51a59e848e80f0e9f7a91c44c5aee87af962a..b153ac8b2f174fd1f7894a95f23d655e39de4a7b 100644
--- a/src/main/java/net/minecraft/server/network/LoginListener.java
+++ b/src/main/java/net/minecraft/server/network/LoginListener.java
@@ -48,6 +48,7 @@ import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
 import org.bukkit.event.player.PlayerPreLoginEvent;
 // CraftBukkit end
 import io.netty.buffer.Unpooled; // Paper
+import net.minecraft.server.MCUtil;
 
 public class LoginListener implements PacketLoginInListener {
 
@@ -127,6 +128,7 @@ public class LoginListener implements PacketLoginInListener {
     }
 
     // Paper start - Cache authenticator threads
+    /*
     private static final AtomicInteger threadId = new AtomicInteger(0);
     private static final java.util.concurrent.ExecutorService authenticatorPool = java.util.concurrent.Executors.newCachedThreadPool(
             r -> { // Yatopia start - make sure produced threads are daemon ones
@@ -135,6 +137,7 @@ public class LoginListener implements PacketLoginInListener {
                 return thread;
             } // Yatopia end
     );
+    */
     // Paper end
     // Spigot start
     public void initUUID()
@@ -224,7 +227,7 @@ public class LoginListener implements PacketLoginInListener {
             // Paper end
             // Spigot start
             // Paper start - Cache authenticator threads
-            authenticatorPool.execute(new Runnable() {
+            MCUtil.asyncExecutor.execute(new Runnable() {
                 @Override
                 public void run() {
                     try {
@@ -266,7 +269,7 @@ public class LoginListener implements PacketLoginInListener {
         }
 
         // Paper start - Cache authenticator threads
-        authenticatorPool.execute(new Runnable() {
+        MCUtil.asyncExecutor.execute(new Runnable() {
             public void run() {
                 GameProfile gameprofile = LoginListener.this.profile_;
 
@@ -394,7 +397,7 @@ public class LoginListener implements PacketLoginInListener {
             this.setGameProfile(com.destroystokyo.paper.proxy.VelocityProxy.createProfile(buf));
 
             // Proceed with login
-            authenticatorPool.execute(() -> {
+            MCUtil.asyncExecutor.execute(() -> {
                 try {
                     new LoginHandler().fireEvents();
                 } catch (Exception ex) {
diff --git a/src/main/java/net/minecraft/world/level/block/entity/TileEntitySkull.java b/src/main/java/net/minecraft/world/level/block/entity/TileEntitySkull.java
index d84ad8afbd5e9bc21c425c5320c604a51b7e1451..62315ccc1a1bdbe3ad9b88c28c52b350f13dbc4b 100644
--- a/src/main/java/net/minecraft/world/level/block/entity/TileEntitySkull.java
+++ b/src/main/java/net/minecraft/world/level/block/entity/TileEntitySkull.java
@@ -33,6 +33,7 @@ import java.util.concurrent.TimeUnit;
 import net.minecraft.server.MinecraftServer;
 import net.minecraft.world.entity.player.EntityHuman;
 // Spigot end
+import net.minecraft.server.MCUtil;
 
 public class TileEntitySkull extends TileEntity /*implements ITickable*/ { // Paper - remove tickable
 
@@ -45,11 +46,13 @@ public class TileEntitySkull extends TileEntity /*implements ITickable*/ { // Pa
     private int ticksPowered_;
     private boolean powered_;
     // Spigot start
+    /*
     public static final ExecutorService executor = Executors.newFixedThreadPool(3,
             new ThreadFactoryBuilder()
                     .setNameFormat("Head Conversion Thread - %1$d")
                     .build()
     );
+    */
     public static final LoadingCache<String, GameProfile> skinCache = CacheBuilder.newBuilder()
             .maximumSize( 5000 )
             .expireAfterAccess( 60, TimeUnit.MINUTES )
@@ -261,7 +264,7 @@ public class TileEntitySkull extends TileEntity /*implements ITickable*/ { // Pa
                             throw new RuntimeException(ex); // Not possible
                         }
                     } else {
-                        return executor.submit(callable);
+                        return MCUtil.asyncExecutor.submit(callable);
                     }
                 }
             }
diff --git a/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java b/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
index 3c1992e212a6d6f1db4d5b807b38d71913619fc0..2301ccbbe5fbc3cf1513ccdc32bb2c22e0ce0aab 100644
--- a/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
+++ b/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
@@ -32,23 +32,30 @@ import java.util.Iterator;
 import java.util.List;
 import java.util.concurrent.Executor;
 import java.util.concurrent.Executors;
+/*
 import java.util.concurrent.SynchronousQueue;
 import java.util.concurrent.ThreadPoolExecutor;
 import java.util.concurrent.TimeUnit;
+*/
+import net.minecraft.server.MCUtil;
 
 public class CraftAsyncScheduler extends CraftScheduler {
 
+    /*
     private final ThreadPoolExecutor executor = new ThreadPoolExecutor(
             4, Integer.MAX_VALUE,30L, TimeUnit.SECONDS, new SynchronousQueue<>(),
             new ThreadFactoryBuilder().setNameFormat("Craft Scheduler Thread - %1$d").build());
+    */
     private final Executor management = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder()
             .setNameFormat("Craft Async Scheduler Management Thread").build());
     private final List<CraftTask> temp = new ArrayList<>();
 
     CraftAsyncScheduler() {
         super(true);
+        /*
         executor.allowCoreThreadTimeOut(true);
         executor.prestartAllCoreThreads();
+        */
     }
 
     @Override
@@ -93,7 +100,7 @@ public class CraftAsyncScheduler extends CraftScheduler {
     private boolean executeTask(CraftTask task) {
         if (isValid(task)) {
             this.runners.put(task.getTaskId(), task);
-            this.executor.execute(new ServerSchedulerReportingWrapper(task));
+            MCUtil.asyncExecutor.execute(new ServerSchedulerReportingWrapper(task));
             return true;
         }
         return false;
diff --git a/src/main/java/org/spigotmc/WatchdogThread.java b/src/main/java/org/spigotmc/WatchdogThread.java
index 001c8dff8a4f32935f457180b83275511d16b31c..f85106571cb673b8ccf36389c82132fd2991cf24 100644
--- a/src/main/java/org/spigotmc/WatchdogThread.java
+++ b/src/main/java/org/spigotmc/WatchdogThread.java
@@ -151,6 +151,7 @@ final public class WatchdogThread extends Thread
     {
         while ( !stopping )
         {
+            net.minecraft.server.MCUtil.flushAsyncTasks();
             // Paper start
             Logger log = Bukkit.getServer().getLogger();
             long currentTime = monotonicMillis();
